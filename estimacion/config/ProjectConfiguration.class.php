<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
//require_once '/usr/share/php/symfony/symfony14/lib/autoload/sfCoreAutoload.class.php';

sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    $this->enablePlugins('sfDoctrinePlugin');
    $this->enablePlugins('sfMondongoPlugin');
    $this->enablePlugins('ullCorePlugin');
    $this->enablePlugins('sfFormExtraPlugin');
  }

}

<?php

/**
 * Base class of repository of Historias document.
 */
abstract class BaseHistoriasRepository extends \Mondongo\Repository
{


    protected $documentClass = 'Historias';


    protected $connectionName = 'mondongo';


    protected $collectionName = 'historias';


    protected $isFile = false;

    /**
     * Ensure indexes.
     *
     * @return void
     */
    public function ensureIndexes()
    {
        $this->getCollection()->ensureIndex(array(
            'identifier' => 1,
        ), array(
            'unique' => 1,
            'safe' => true,
        ));

    }
}
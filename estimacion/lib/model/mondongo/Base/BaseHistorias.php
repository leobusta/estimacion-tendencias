<?php

/**
 * Base class of Historias document.
 */
abstract class BaseHistorias extends \Mondongo\Document\Document implements \ArrayAccess
{


    protected $data = array(
        'fields' => array(
            'proyectos_id' => null,
            'autor' => null,
            'modulo' => null,
            'nombre_hu' => null,
            'identificador' => null,
            'dependencia' => null,
            'responsables' => null,
            'actores' => null,
            'iteracion' => null,
            'tiempo_estimado' => null,
            'tiempo_real' => null,
            'descripcion' => null,
            'observaciones' => null,
            'created_at' => null,
            'updated_at' => null,
            'identifier' => null,
        ),
    );


    protected $fieldsModified = array(

    );


    static protected $dataCamelCaseMap = array(
        'proyectos_id' => 'ProyectosId',
        'autor' => 'Autor',
        'modulo' => 'Modulo',
        'nombre_hu' => 'NombreHu',
        'identificador' => 'Identificador',
        'dependencia' => 'Dependencia',
        'responsables' => 'Responsables',
        'actores' => 'Actores',
        'iteracion' => 'Iteracion',
        'tiempo_estimado' => 'TiempoEstimado',
        'tiempo_real' => 'TiempoReal',
        'descripcion' => 'Descripcion',
        'observaciones' => 'Observaciones',
        'created_at' => 'CreatedAt',
        'updated_at' => 'UpdatedAt',
        'identifier' => 'Identifier',
    );

    /**
     * Returns the Mondongo of the document.
     *
     * @return Mondongo\Mondongo The Mondongo of the document.
     */
    public function getMondongo()
    {
        return \Mondongo\Container::getForDocumentClass('Historias');
    }

    /**
     * Returns the repository of the document.
     *
     * @return Mondongo\Repository The repository of the document.
     */
    public function getRepository()
    {
        return $this->getMondongo()->getRepository('Historias');
    }


    protected function updateTimestampableCreated()
    {
        $this->setCreatedAt(new \DateTime());
    }


    protected function updateTimestampableUpdated()
    {
        $this->setUpdatedAt(new \DateTime());
    }


    protected function updateIdentifierAutoIncrement()
    {
        $last = $this->getRepository()
            ->getCollection()
            ->find(array(), array('identifier' => 1))
            ->sort(array('identifier' => -1))
            ->limit(1)
            ->getNext()
        ;

        $identifier = null !== $last ? $last['identifier'] + 1 : 1;

        $this->setIdentifier($identifier);
    }

    /**
     * Set the data in the document (hydrate).
     *
     * @return void
     */
    public function setDocumentData($data)
    {
        $this->id = $data['_id'];

        if (isset($data['proyectos_id'])) {
            $this->data['fields']['proyectos_id'] = (int) $data['proyectos_id'];
        }
        if (isset($data['autor'])) {
            $this->data['fields']['autor'] = (string) $data['autor'];
        }
        if (isset($data['modulo'])) {
            $this->data['fields']['modulo'] = (string) $data['modulo'];
        }
        if (isset($data['nombre_hu'])) {
            $this->data['fields']['nombre_hu'] = (string) $data['nombre_hu'];
        }
        if (isset($data['identificador'])) {
            $this->data['fields']['identificador'] = (string) $data['identificador'];
        }
        if (isset($data['dependencia'])) {
            $this->data['fields']['dependencia'] = (string) $data['dependencia'];
        }
        if (isset($data['responsables'])) {
            $this->data['fields']['responsables'] = (string) $data['responsables'];
        }
        if (isset($data['actores'])) {
            $this->data['fields']['actores'] = (string) $data['actores'];
        }
        if (isset($data['iteracion'])) {
            $this->data['fields']['iteracion'] = (int) $data['iteracion'];
        }
        if (isset($data['tiempo_estimado'])) {
            $this->data['fields']['tiempo_estimado'] = (int) $data['tiempo_estimado'];
        }
        if (isset($data['tiempo_real'])) {
            $this->data['fields']['tiempo_real'] = (int) $data['tiempo_real'];
        }
        if (isset($data['descripcion'])) {
            $this->data['fields']['descripcion'] = (string) $data['descripcion'];
        }
        if (isset($data['observaciones'])) {
            $this->data['fields']['observaciones'] = (string) $data['observaciones'];
        }
        if (isset($data['created_at'])) {
            $date = new \DateTime(); $date->setTimestamp($data['created_at']->sec); $this->data['fields']['created_at'] = $date;
        }
        if (isset($data['updated_at'])) {
            $date = new \DateTime(); $date->setTimestamp($data['updated_at']->sec); $this->data['fields']['updated_at'] = $date;
        }
        if (isset($data['identifier'])) {
            $this->data['fields']['identifier'] = (int) $data['identifier'];
        }


        
    }

    /**
     * Convert an array of fields with data to Mongo values.
     *
     * @param array $fields An array of fields with data.
     *
     * @return array The fields with data in Mongo values.
     */
    public function fieldsToMongo($fields)
    {
        if (isset($fields['proyectos_id'])) {
            $fields['proyectos_id'] = (int) $fields['proyectos_id'];
        }
        if (isset($fields['autor'])) {
            $fields['autor'] = (string) $fields['autor'];
        }
        if (isset($fields['modulo'])) {
            $fields['modulo'] = (string) $fields['modulo'];
        }
        if (isset($fields['nombre_hu'])) {
            $fields['nombre_hu'] = (string) $fields['nombre_hu'];
        }
        if (isset($fields['identificador'])) {
            $fields['identificador'] = (string) $fields['identificador'];
        }
        if (isset($fields['dependencia'])) {
            $fields['dependencia'] = (string) $fields['dependencia'];
        }
        if (isset($fields['responsables'])) {
            $fields['responsables'] = (string) $fields['responsables'];
        }
        if (isset($fields['actores'])) {
            $fields['actores'] = (string) $fields['actores'];
        }
        if (isset($fields['iteracion'])) {
            $fields['iteracion'] = (int) $fields['iteracion'];
        }
        if (isset($fields['tiempo_estimado'])) {
            $fields['tiempo_estimado'] = (int) $fields['tiempo_estimado'];
        }
        if (isset($fields['tiempo_real'])) {
            $fields['tiempo_real'] = (int) $fields['tiempo_real'];
        }
        if (isset($fields['descripcion'])) {
            $fields['descripcion'] = (string) $fields['descripcion'];
        }
        if (isset($fields['observaciones'])) {
            $fields['observaciones'] = (string) $fields['observaciones'];
        }
        if (isset($fields['created_at'])) {
            if ($fields['created_at'] instanceof \DateTime) { $fields['created_at'] = $fields['created_at']->getTimestamp(); } elseif (is_string($fields['created_at'])) { $fields['created_at'] = strtotime($fields['created_at']); } $fields['created_at'] = new \MongoDate($fields['created_at']);
        }
        if (isset($fields['updated_at'])) {
            if ($fields['updated_at'] instanceof \DateTime) { $fields['updated_at'] = $fields['updated_at']->getTimestamp(); } elseif (is_string($fields['updated_at'])) { $fields['updated_at'] = strtotime($fields['updated_at']); } $fields['updated_at'] = new \MongoDate($fields['updated_at']);
        }
        if (isset($fields['identifier'])) {
            $fields['identifier'] = (int) $fields['identifier'];
        }


        return $fields;
    }

    /**
     * Set the "proyectos_id" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setProyectosId($value)
    {
        if (!array_key_exists('proyectos_id', $this->fieldsModified)) {
            $this->fieldsModified['proyectos_id'] = $this->data['fields']['proyectos_id'];
        } elseif ($value === $this->fieldsModified['proyectos_id']) {
            unset($this->fieldsModified['proyectos_id']);
        }

        $this->data['fields']['proyectos_id'] = $value;
    }

    /**
     * Returns the "proyectos_id" field.
     *
     * @return mixed The proyectos_id field.
     */
    public function getProyectosId()
    {
        return $this->data['fields']['proyectos_id'];
    }

    /**
     * Set the "autor" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setAutor($value)
    {
        if (!array_key_exists('autor', $this->fieldsModified)) {
            $this->fieldsModified['autor'] = $this->data['fields']['autor'];
        } elseif ($value === $this->fieldsModified['autor']) {
            unset($this->fieldsModified['autor']);
        }

        $this->data['fields']['autor'] = $value;
    }

    /**
     * Returns the "autor" field.
     *
     * @return mixed The autor field.
     */
    public function getAutor()
    {
        return $this->data['fields']['autor'];
    }

    /**
     * Set the "modulo" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setModulo($value)
    {
        if (!array_key_exists('modulo', $this->fieldsModified)) {
            $this->fieldsModified['modulo'] = $this->data['fields']['modulo'];
        } elseif ($value === $this->fieldsModified['modulo']) {
            unset($this->fieldsModified['modulo']);
        }

        $this->data['fields']['modulo'] = $value;
    }

    /**
     * Returns the "modulo" field.
     *
     * @return mixed The modulo field.
     */
    public function getModulo()
    {
        return $this->data['fields']['modulo'];
    }

    /**
     * Set the "nombre_hu" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setNombreHu($value)
    {
        if (!array_key_exists('nombre_hu', $this->fieldsModified)) {
            $this->fieldsModified['nombre_hu'] = $this->data['fields']['nombre_hu'];
        } elseif ($value === $this->fieldsModified['nombre_hu']) {
            unset($this->fieldsModified['nombre_hu']);
        }

        $this->data['fields']['nombre_hu'] = $value;
    }

    /**
     * Returns the "nombre_hu" field.
     *
     * @return mixed The nombre_hu field.
     */
    public function getNombreHu()
    {
        return $this->data['fields']['nombre_hu'];
    }

    /**
     * Set the "identificador" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setIdentificador($value)
    {
        if (!array_key_exists('identificador', $this->fieldsModified)) {
            $this->fieldsModified['identificador'] = $this->data['fields']['identificador'];
        } elseif ($value === $this->fieldsModified['identificador']) {
            unset($this->fieldsModified['identificador']);
        }

        $this->data['fields']['identificador'] = $value;
    }

    /**
     * Returns the "identificador" field.
     *
     * @return mixed The identificador field.
     */
    public function getIdentificador()
    {
        return $this->data['fields']['identificador'];
    }

    /**
     * Set the "dependencia" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setDependencia($value)
    {
        if (!array_key_exists('dependencia', $this->fieldsModified)) {
            $this->fieldsModified['dependencia'] = $this->data['fields']['dependencia'];
        } elseif ($value === $this->fieldsModified['dependencia']) {
            unset($this->fieldsModified['dependencia']);
        }

        $this->data['fields']['dependencia'] = $value;
    }

    /**
     * Returns the "dependencia" field.
     *
     * @return mixed The dependencia field.
     */
    public function getDependencia()
    {
        return $this->data['fields']['dependencia'];
    }

    /**
     * Set the "responsables" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setResponsables($value)
    {
        if (!array_key_exists('responsables', $this->fieldsModified)) {
            $this->fieldsModified['responsables'] = $this->data['fields']['responsables'];
        } elseif ($value === $this->fieldsModified['responsables']) {
            unset($this->fieldsModified['responsables']);
        }

        $this->data['fields']['responsables'] = $value;
    }

    /**
     * Returns the "responsables" field.
     *
     * @return mixed The responsables field.
     */
    public function getResponsables()
    {
        return $this->data['fields']['responsables'];
    }

    /**
     * Set the "actores" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setActores($value)
    {
        if (!array_key_exists('actores', $this->fieldsModified)) {
            $this->fieldsModified['actores'] = $this->data['fields']['actores'];
        } elseif ($value === $this->fieldsModified['actores']) {
            unset($this->fieldsModified['actores']);
        }

        $this->data['fields']['actores'] = $value;
    }

    /**
     * Returns the "actores" field.
     *
     * @return mixed The actores field.
     */
    public function getActores()
    {
        return $this->data['fields']['actores'];
    }

    /**
     * Set the "iteracion" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setIteracion($value)
    {
        if (!array_key_exists('iteracion', $this->fieldsModified)) {
            $this->fieldsModified['iteracion'] = $this->data['fields']['iteracion'];
        } elseif ($value === $this->fieldsModified['iteracion']) {
            unset($this->fieldsModified['iteracion']);
        }

        $this->data['fields']['iteracion'] = $value;
    }

    /**
     * Returns the "iteracion" field.
     *
     * @return mixed The iteracion field.
     */
    public function getIteracion()
    {
        return $this->data['fields']['iteracion'];
    }

    /**
     * Set the "tiempo_estimado" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setTiempoEstimado($value)
    {
        if (!array_key_exists('tiempo_estimado', $this->fieldsModified)) {
            $this->fieldsModified['tiempo_estimado'] = $this->data['fields']['tiempo_estimado'];
        } elseif ($value === $this->fieldsModified['tiempo_estimado']) {
            unset($this->fieldsModified['tiempo_estimado']);
        }

        $this->data['fields']['tiempo_estimado'] = $value;
    }

    /**
     * Returns the "tiempo_estimado" field.
     *
     * @return mixed The tiempo_estimado field.
     */
    public function getTiempoEstimado()
    {
        return $this->data['fields']['tiempo_estimado'];
    }

    /**
     * Set the "tiempo_real" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setTiempoReal($value)
    {
        if (!array_key_exists('tiempo_real', $this->fieldsModified)) {
            $this->fieldsModified['tiempo_real'] = $this->data['fields']['tiempo_real'];
        } elseif ($value === $this->fieldsModified['tiempo_real']) {
            unset($this->fieldsModified['tiempo_real']);
        }

        $this->data['fields']['tiempo_real'] = $value;
    }

    /**
     * Returns the "tiempo_real" field.
     *
     * @return mixed The tiempo_real field.
     */
    public function getTiempoReal()
    {
        return $this->data['fields']['tiempo_real'];
    }

    /**
     * Set the "descripcion" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setDescripcion($value)
    {
        if (!array_key_exists('descripcion', $this->fieldsModified)) {
            $this->fieldsModified['descripcion'] = $this->data['fields']['descripcion'];
        } elseif ($value === $this->fieldsModified['descripcion']) {
            unset($this->fieldsModified['descripcion']);
        }

        $this->data['fields']['descripcion'] = $value;
    }

    /**
     * Returns the "descripcion" field.
     *
     * @return mixed The descripcion field.
     */
    public function getDescripcion()
    {
        return $this->data['fields']['descripcion'];
    }

    /**
     * Set the "observaciones" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setObservaciones($value)
    {
        if (!array_key_exists('observaciones', $this->fieldsModified)) {
            $this->fieldsModified['observaciones'] = $this->data['fields']['observaciones'];
        } elseif ($value === $this->fieldsModified['observaciones']) {
            unset($this->fieldsModified['observaciones']);
        }

        $this->data['fields']['observaciones'] = $value;
    }

    /**
     * Returns the "observaciones" field.
     *
     * @return mixed The observaciones field.
     */
    public function getObservaciones()
    {
        return $this->data['fields']['observaciones'];
    }

    /**
     * Set the "created_at" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setCreatedAt($value)
    {
        if (!array_key_exists('created_at', $this->fieldsModified)) {
            $this->fieldsModified['created_at'] = $this->data['fields']['created_at'];
        } elseif ($value === $this->fieldsModified['created_at']) {
            unset($this->fieldsModified['created_at']);
        }

        $this->data['fields']['created_at'] = $value;
    }

    /**
     * Returns the "created_at" field.
     *
     * @return mixed The created_at field.
     */
    public function getCreatedAt()
    {
        return $this->data['fields']['created_at'];
    }

    /**
     * Set the "updated_at" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setUpdatedAt($value)
    {
        if (!array_key_exists('updated_at', $this->fieldsModified)) {
            $this->fieldsModified['updated_at'] = $this->data['fields']['updated_at'];
        } elseif ($value === $this->fieldsModified['updated_at']) {
            unset($this->fieldsModified['updated_at']);
        }

        $this->data['fields']['updated_at'] = $value;
    }

    /**
     * Returns the "updated_at" field.
     *
     * @return mixed The updated_at field.
     */
    public function getUpdatedAt()
    {
        return $this->data['fields']['updated_at'];
    }

    /**
     * Set the "identifier" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setIdentifier($value)
    {
        if (!array_key_exists('identifier', $this->fieldsModified)) {
            $this->fieldsModified['identifier'] = $this->data['fields']['identifier'];
        } elseif ($value === $this->fieldsModified['identifier']) {
            unset($this->fieldsModified['identifier']);
        }

        $this->data['fields']['identifier'] = $value;
    }

    /**
     * Returns the "identifier" field.
     *
     * @return mixed The identifier field.
     */
    public function getIdentifier()
    {
        return $this->data['fields']['identifier'];
    }


    public function preInsertExtensions()
    {
        $this->updateTimestampableCreated();
        $this->updateIdentifierAutoIncrement();

    }


    public function postInsertExtensions()
    {

    }


    public function preUpdateExtensions()
    {
        $this->updateTimestampableUpdated();

    }


    public function postUpdateExtensions()
    {

    }


    public function preSaveExtensions()
    {

    }


    public function postSaveExtensions()
    {

    }


    public function preDeleteExtensions()
    {

    }


    public function postDeleteExtensions()
    {

    }

    /**
     * Returns the data CamelCase map.
     *
     * @return array The data CamelCase map.
     */
    static public function getDataCamelCaseMap()
    {
        return self::$dataCamelCaseMap;
    }

    /**
     * Import data from an array.
     *
     * @param array $array An array.
     *
     * @return void
     */
    public function fromArray($array)
    {
        if (isset($array['proyectos_id'])) {
            $this->setProyectosId($array['proyectos_id']);
        }
        if (isset($array['autor'])) {
            $this->setAutor($array['autor']);
        }
        if (isset($array['modulo'])) {
            $this->setModulo($array['modulo']);
        }
        if (isset($array['nombre_hu'])) {
            $this->setNombreHu($array['nombre_hu']);
        }
        if (isset($array['identificador'])) {
            $this->setIdentificador($array['identificador']);
        }
        if (isset($array['dependencia'])) {
            $this->setDependencia($array['dependencia']);
        }
        if (isset($array['responsables'])) {
            $this->setResponsables($array['responsables']);
        }
        if (isset($array['actores'])) {
            $this->setActores($array['actores']);
        }
        if (isset($array['iteracion'])) {
            $this->setIteracion($array['iteracion']);
        }
        if (isset($array['tiempo_estimado'])) {
            $this->setTiempoEstimado($array['tiempo_estimado']);
        }
        if (isset($array['tiempo_real'])) {
            $this->setTiempoReal($array['tiempo_real']);
        }
        if (isset($array['descripcion'])) {
            $this->setDescripcion($array['descripcion']);
        }
        if (isset($array['observaciones'])) {
            $this->setObservaciones($array['observaciones']);
        }
        if (isset($array['created_at'])) {
            $this->setCreatedAt($array['created_at']);
        }
        if (isset($array['updated_at'])) {
            $this->setUpdatedAt($array['updated_at']);
        }
        if (isset($array['identifier'])) {
            $this->setIdentifier($array['identifier']);
        }

    }

    /**
     * Export the document data to array.
     *
     * @param bool $withEmbeddeds If export embeddeds or not.
     *
     * @return array An array with the document data.
     */
    public function toArray($withEmbeddeds = true)
    {
        $array = array();

        if (null !== $this->data['fields']['proyectos_id']) {
            $array['proyectos_id'] = $this->data['fields']['proyectos_id'];
        }
        if (null !== $this->data['fields']['autor']) {
            $array['autor'] = $this->data['fields']['autor'];
        }
        if (null !== $this->data['fields']['modulo']) {
            $array['modulo'] = $this->data['fields']['modulo'];
        }
        if (null !== $this->data['fields']['nombre_hu']) {
            $array['nombre_hu'] = $this->data['fields']['nombre_hu'];
        }
        if (null !== $this->data['fields']['identificador']) {
            $array['identificador'] = $this->data['fields']['identificador'];
        }
        if (null !== $this->data['fields']['dependencia']) {
            $array['dependencia'] = $this->data['fields']['dependencia'];
        }
        if (null !== $this->data['fields']['responsables']) {
            $array['responsables'] = $this->data['fields']['responsables'];
        }
        if (null !== $this->data['fields']['actores']) {
            $array['actores'] = $this->data['fields']['actores'];
        }
        if (null !== $this->data['fields']['iteracion']) {
            $array['iteracion'] = $this->data['fields']['iteracion'];
        }
        if (null !== $this->data['fields']['tiempo_estimado']) {
            $array['tiempo_estimado'] = $this->data['fields']['tiempo_estimado'];
        }
        if (null !== $this->data['fields']['tiempo_real']) {
            $array['tiempo_real'] = $this->data['fields']['tiempo_real'];
        }
        if (null !== $this->data['fields']['descripcion']) {
            $array['descripcion'] = $this->data['fields']['descripcion'];
        }
        if (null !== $this->data['fields']['observaciones']) {
            $array['observaciones'] = $this->data['fields']['observaciones'];
        }
        if (null !== $this->data['fields']['created_at']) {
            $array['created_at'] = $this->data['fields']['created_at'];
        }
        if (null !== $this->data['fields']['updated_at']) {
            $array['updated_at'] = $this->data['fields']['updated_at'];
        }
        if (null !== $this->data['fields']['identifier']) {
            $array['identifier'] = $this->data['fields']['identifier'];
        }


        if ($withEmbeddeds) {

        }

        return $array;
    }

    /**
     * Throws an \LogicException because you cannot check if data exists.
     *
     * @throws \LogicException
     */
    public function offsetExists($name)
    {
        throw new \LogicException('You cannot check if data exists in a document.');
    }

    /**
     * Set data in the document.
     *
     * @param string $name  The data name.
     * @param mixed  $value The value.
     *
     * @return void
     *
     * @throws \InvalidArgumentException If the data name does not exists.
     */
    public function offsetSet($name, $value)
    {
        if (!isset(self::$dataCamelCaseMap[$name])) {
            throw new \InvalidArgumentException(sprintf('The name "%s" does not exists.', $name));
        }

        $method = 'set'.self::$dataCamelCaseMap[$name];

        $this->$method($value);
    }

    /**
     * Returns data of the document.
     *
     * @param string $name The data name.
     *
     * @return mixed Some data.
     *
     * @throws \InvalidArgumentException If the data name does not exists.
     */
    public function offsetGet($name)
    {
        if (!isset(self::$dataCamelCaseMap[$name])) {
            throw new \InvalidArgumentException(sprintf('The data "%s" does not exists.', $name));
        }

        $method = 'get'.self::$dataCamelCaseMap[$name];

        return $this->$method();
    }

    /**
     * Throws a \LogicException because you cannot unset data in the document.
     *
     * @throws \LogicException
     */
    public function offsetUnset($name)
    {
        throw new \LogicException('You cannot unset data in the document.');
    }

    /**
     * Set data in the document.
     *
     * @param string $name  The data name.
     * @param mixed  $value The value.
     *
     * @return void
     *
     * @throws \InvalidArgumentException If the data name does not exists.
     */
    public function __set($name, $value)
    {
        if (!isset(self::$dataCamelCaseMap[$name])) {
            throw new \InvalidArgumentException(sprintf('The name "%s" does not exists.', $name));
        }

        $method = 'set'.self::$dataCamelCaseMap[$name];

        $this->$method($value);
    }

    /**
     * Returns data of the document.
     *
     * @param string $name The data name.
     *
     * @return mixed Some data.
     *
     * @throws \InvalidArgumentException If the data name does not exists.
     */
    public function __get($name)
    {
        if (!isset(self::$dataCamelCaseMap[$name])) {
            throw new \InvalidArgumentException(sprintf('The data "%s" does not exists.', $name));
        }

        $method = 'get'.self::$dataCamelCaseMap[$name];

        return $this->$method();
    }

    /**
     * Returns the data map.
     *
     * @return array The data map.
     */
    static public function getDataMap()
    {
        return array(
            'fields' => array(
                'proyectos_id' => array(
                    'type' => 'integer',
                    'required' => true,
                ),
                'autor' => array(
                    'type' => 'string',
                    'required' => true,
                ),
                'modulo' => array(
                    'type' => 'string',
                    'required' => true,
                ),
                'nombre_hu' => array(
                    'type' => 'string',
                    'required' => true,
                ),
                'identificador' => array(
                    'type' => 'string',
                    'required' => true,
                ),
                'dependencia' => array(
                    'type' => 'string',
                    'required' => false,
                ),
                'responsables' => array(
                    'type' => 'string',
                    'required' => false,
                ),
                'actores' => array(
                    'type' => 'string',
                    'required' => false,
                ),
                'iteracion' => array(
                    'type' => 'integer',
                    'required' => false,
                ),
                'tiempo_estimado' => array(
                    'type' => 'integer',
                    'required' => false,
                ),
                'tiempo_real' => array(
                    'type' => 'integer',
                    'required' => false,
                ),
                'descripcion' => array(
                    'type' => 'string',
                    'required' => false,
                ),
                'observaciones' => array(
                    'type' => 'string',
                    'required' => false,
                ),
                'created_at' => array(
                    'type' => 'date',
                ),
                'updated_at' => array(
                    'type' => 'date',
                ),
                'identifier' => array(
                    'type' => 'integer',
                ),
            ),
            'references' => array(

            ),
            'embeddeds' => array(

            ),
            'relations' => array(

            ),
        );
    }
}
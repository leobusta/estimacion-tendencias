<?php

/**
 * Base class of repository of Usuarios document.
 */
abstract class BaseUsuariosRepository extends \Mondongo\Repository
{


    protected $documentClass = 'Usuarios';


    protected $connectionName = 'mondongo';


    protected $collectionName = 'usuarios';


    protected $isFile = false;

    /**
     * Ensure indexes.
     *
     * @return void
     */
    public function ensureIndexes()
    {
        $this->getCollection()->ensureIndex(array(
            'identifier' => 1,
        ), array(
            'unique' => 1,
            'safe' => true,
        ));

    }
}
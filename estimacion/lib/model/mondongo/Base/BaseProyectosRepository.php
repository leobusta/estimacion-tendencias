<?php

/**
 * Base class of repository of Proyectos document.
 */
abstract class BaseProyectosRepository extends \Mondongo\Repository
{


    protected $documentClass = 'Proyectos';


    protected $connectionName = 'mondongo';


    protected $collectionName = 'proyectos';


    protected $isFile = false;

    /**
     * Ensure indexes.
     *
     * @return void
     */
    public function ensureIndexes()
    {
        $this->getCollection()->ensureIndex(array(
            'identifier' => 1,
        ), array(
            'unique' => 1,
            'safe' => true,
        ));

    }
}
<?php

/**
 * Base class of Proyectos document.
 */
abstract class BaseProyectos extends \Mondongo\Document\Document implements \ArrayAccess
{


    protected $data = array(
        'fields' => array(
            'usuarios_id' => null,
            'nombre' => null,
            'descripcion' => null,
            'activo' => null,
            'created_at' => null,
            'updated_at' => null,
            'identifier' => null,
        ),
    );


    protected $fieldsModified = array(

    );


    static protected $dataCamelCaseMap = array(
        'usuarios_id' => 'UsuariosId',
        'nombre' => 'Nombre',
        'descripcion' => 'Descripcion',
        'activo' => 'Activo',
        'created_at' => 'CreatedAt',
        'updated_at' => 'UpdatedAt',
        'identifier' => 'Identifier',
    );

    /**
     * Returns the Mondongo of the document.
     *
     * @return Mondongo\Mondongo The Mondongo of the document.
     */
    public function getMondongo()
    {
        return \Mondongo\Container::getForDocumentClass('Proyectos');
    }

    /**
     * Returns the repository of the document.
     *
     * @return Mondongo\Repository The repository of the document.
     */
    public function getRepository()
    {
        return $this->getMondongo()->getRepository('Proyectos');
    }


    protected function updateTimestampableCreated()
    {
        $this->setCreatedAt(new \DateTime());
    }


    protected function updateTimestampableUpdated()
    {
        $this->setUpdatedAt(new \DateTime());
    }


    protected function updateIdentifierAutoIncrement()
    {
        $last = $this->getRepository()
            ->getCollection()
            ->find(array(), array('identifier' => 1))
            ->sort(array('identifier' => -1))
            ->limit(1)
            ->getNext()
        ;

        $identifier = null !== $last ? $last['identifier'] + 1 : 1;

        $this->setIdentifier($identifier);
    }

    /**
     * Set the data in the document (hydrate).
     *
     * @return void
     */
    public function setDocumentData($data)
    {
        $this->id = $data['_id'];

        if (isset($data['usuarios_id'])) {
            $this->data['fields']['usuarios_id'] = (int) $data['usuarios_id'];
        }
        if (isset($data['nombre'])) {
            $this->data['fields']['nombre'] = (string) $data['nombre'];
        }
        if (isset($data['descripcion'])) {
            $this->data['fields']['descripcion'] = (string) $data['descripcion'];
        }
        if (isset($data['activo'])) {
            $this->data['fields']['activo'] = (bool) $data['activo'];
        }
        if (isset($data['created_at'])) {
            $date = new \DateTime(); $date->setTimestamp($data['created_at']->sec); $this->data['fields']['created_at'] = $date;
        }
        if (isset($data['updated_at'])) {
            $date = new \DateTime(); $date->setTimestamp($data['updated_at']->sec); $this->data['fields']['updated_at'] = $date;
        }
        if (isset($data['identifier'])) {
            $this->data['fields']['identifier'] = (int) $data['identifier'];
        }


        
    }

    /**
     * Convert an array of fields with data to Mongo values.
     *
     * @param array $fields An array of fields with data.
     *
     * @return array The fields with data in Mongo values.
     */
    public function fieldsToMongo($fields)
    {
        if (isset($fields['usuarios_id'])) {
            $fields['usuarios_id'] = (int) $fields['usuarios_id'];
        }
        if (isset($fields['nombre'])) {
            $fields['nombre'] = (string) $fields['nombre'];
        }
        if (isset($fields['descripcion'])) {
            $fields['descripcion'] = (string) $fields['descripcion'];
        }
        if (isset($fields['activo'])) {
            $fields['activo'] = (bool) $fields['activo'];
        }
        if (isset($fields['created_at'])) {
            if ($fields['created_at'] instanceof \DateTime) { $fields['created_at'] = $fields['created_at']->getTimestamp(); } elseif (is_string($fields['created_at'])) { $fields['created_at'] = strtotime($fields['created_at']); } $fields['created_at'] = new \MongoDate($fields['created_at']);
        }
        if (isset($fields['updated_at'])) {
            if ($fields['updated_at'] instanceof \DateTime) { $fields['updated_at'] = $fields['updated_at']->getTimestamp(); } elseif (is_string($fields['updated_at'])) { $fields['updated_at'] = strtotime($fields['updated_at']); } $fields['updated_at'] = new \MongoDate($fields['updated_at']);
        }
        if (isset($fields['identifier'])) {
            $fields['identifier'] = (int) $fields['identifier'];
        }


        return $fields;
    }

    /**
     * Set the "usuarios_id" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setUsuariosId($value)
    {
        if (!array_key_exists('usuarios_id', $this->fieldsModified)) {
            $this->fieldsModified['usuarios_id'] = $this->data['fields']['usuarios_id'];
        } elseif ($value === $this->fieldsModified['usuarios_id']) {
            unset($this->fieldsModified['usuarios_id']);
        }

        $this->data['fields']['usuarios_id'] = $value;
    }

    /**
     * Returns the "usuarios_id" field.
     *
     * @return mixed The usuarios_id field.
     */
    public function getUsuariosId()
    {
        return $this->data['fields']['usuarios_id'];
    }

    /**
     * Set the "nombre" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setNombre($value)
    {
        if (!array_key_exists('nombre', $this->fieldsModified)) {
            $this->fieldsModified['nombre'] = $this->data['fields']['nombre'];
        } elseif ($value === $this->fieldsModified['nombre']) {
            unset($this->fieldsModified['nombre']);
        }

        $this->data['fields']['nombre'] = $value;
    }

    /**
     * Returns the "nombre" field.
     *
     * @return mixed The nombre field.
     */
    public function getNombre()
    {
        return $this->data['fields']['nombre'];
    }

    /**
     * Set the "descripcion" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setDescripcion($value)
    {
        if (!array_key_exists('descripcion', $this->fieldsModified)) {
            $this->fieldsModified['descripcion'] = $this->data['fields']['descripcion'];
        } elseif ($value === $this->fieldsModified['descripcion']) {
            unset($this->fieldsModified['descripcion']);
        }

        $this->data['fields']['descripcion'] = $value;
    }

    /**
     * Returns the "descripcion" field.
     *
     * @return mixed The descripcion field.
     */
    public function getDescripcion()
    {
        return $this->data['fields']['descripcion'];
    }

    /**
     * Set the "activo" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setActivo($value)
    {
        if (!array_key_exists('activo', $this->fieldsModified)) {
            $this->fieldsModified['activo'] = $this->data['fields']['activo'];
        } elseif ($value === $this->fieldsModified['activo']) {
            unset($this->fieldsModified['activo']);
        }

        $this->data['fields']['activo'] = $value;
    }

    /**
     * Returns the "activo" field.
     *
     * @return mixed The activo field.
     */
    public function getActivo()
    {
        return $this->data['fields']['activo'];
    }

    /**
     * Set the "created_at" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setCreatedAt($value)
    {
        if (!array_key_exists('created_at', $this->fieldsModified)) {
            $this->fieldsModified['created_at'] = $this->data['fields']['created_at'];
        } elseif ($value === $this->fieldsModified['created_at']) {
            unset($this->fieldsModified['created_at']);
        }

        $this->data['fields']['created_at'] = $value;
    }

    /**
     * Returns the "created_at" field.
     *
     * @return mixed The created_at field.
     */
    public function getCreatedAt()
    {
        return $this->data['fields']['created_at'];
    }

    /**
     * Set the "updated_at" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setUpdatedAt($value)
    {
        if (!array_key_exists('updated_at', $this->fieldsModified)) {
            $this->fieldsModified['updated_at'] = $this->data['fields']['updated_at'];
        } elseif ($value === $this->fieldsModified['updated_at']) {
            unset($this->fieldsModified['updated_at']);
        }

        $this->data['fields']['updated_at'] = $value;
    }

    /**
     * Returns the "updated_at" field.
     *
     * @return mixed The updated_at field.
     */
    public function getUpdatedAt()
    {
        return $this->data['fields']['updated_at'];
    }

    /**
     * Set the "identifier" field.
     *
     * @param mixed $value The value.
     *
     * @return void
     */
    public function setIdentifier($value)
    {
        if (!array_key_exists('identifier', $this->fieldsModified)) {
            $this->fieldsModified['identifier'] = $this->data['fields']['identifier'];
        } elseif ($value === $this->fieldsModified['identifier']) {
            unset($this->fieldsModified['identifier']);
        }

        $this->data['fields']['identifier'] = $value;
    }

    /**
     * Returns the "identifier" field.
     *
     * @return mixed The identifier field.
     */
    public function getIdentifier()
    {
        return $this->data['fields']['identifier'];
    }


    public function preInsertExtensions()
    {
        $this->updateTimestampableCreated();
        $this->updateIdentifierAutoIncrement();

    }


    public function postInsertExtensions()
    {

    }


    public function preUpdateExtensions()
    {
        $this->updateTimestampableUpdated();

    }


    public function postUpdateExtensions()
    {

    }


    public function preSaveExtensions()
    {

    }


    public function postSaveExtensions()
    {

    }


    public function preDeleteExtensions()
    {

    }


    public function postDeleteExtensions()
    {

    }

    /**
     * Returns the data CamelCase map.
     *
     * @return array The data CamelCase map.
     */
    static public function getDataCamelCaseMap()
    {
        return self::$dataCamelCaseMap;
    }

    /**
     * Import data from an array.
     *
     * @param array $array An array.
     *
     * @return void
     */
    public function fromArray($array)
    {
        if (isset($array['usuarios_id'])) {
            $this->setUsuariosId($array['usuarios_id']);
        }
        if (isset($array['nombre'])) {
            $this->setNombre($array['nombre']);
        }
        if (isset($array['descripcion'])) {
            $this->setDescripcion($array['descripcion']);
        }
        if (isset($array['activo'])) {
            $this->setActivo($array['activo']);
        }
        if (isset($array['created_at'])) {
            $this->setCreatedAt($array['created_at']);
        }
        if (isset($array['updated_at'])) {
            $this->setUpdatedAt($array['updated_at']);
        }
        if (isset($array['identifier'])) {
            $this->setIdentifier($array['identifier']);
        }

    }

    /**
     * Export the document data to array.
     *
     * @param bool $withEmbeddeds If export embeddeds or not.
     *
     * @return array An array with the document data.
     */
    public function toArray($withEmbeddeds = true)
    {
        $array = array();

        if (null !== $this->data['fields']['usuarios_id']) {
            $array['usuarios_id'] = $this->data['fields']['usuarios_id'];
        }
        if (null !== $this->data['fields']['nombre']) {
            $array['nombre'] = $this->data['fields']['nombre'];
        }
        if (null !== $this->data['fields']['descripcion']) {
            $array['descripcion'] = $this->data['fields']['descripcion'];
        }
        if (null !== $this->data['fields']['activo']) {
            $array['activo'] = $this->data['fields']['activo'];
        }
        if (null !== $this->data['fields']['created_at']) {
            $array['created_at'] = $this->data['fields']['created_at'];
        }
        if (null !== $this->data['fields']['updated_at']) {
            $array['updated_at'] = $this->data['fields']['updated_at'];
        }
        if (null !== $this->data['fields']['identifier']) {
            $array['identifier'] = $this->data['fields']['identifier'];
        }


        if ($withEmbeddeds) {

        }

        return $array;
    }

    /**
     * Throws an \LogicException because you cannot check if data exists.
     *
     * @throws \LogicException
     */
    public function offsetExists($name)
    {
        throw new \LogicException('You cannot check if data exists in a document.');
    }

    /**
     * Set data in the document.
     *
     * @param string $name  The data name.
     * @param mixed  $value The value.
     *
     * @return void
     *
     * @throws \InvalidArgumentException If the data name does not exists.
     */
    public function offsetSet($name, $value)
    {
        if (!isset(self::$dataCamelCaseMap[$name])) {
            throw new \InvalidArgumentException(sprintf('The name "%s" does not exists.', $name));
        }

        $method = 'set'.self::$dataCamelCaseMap[$name];

        $this->$method($value);
    }

    /**
     * Returns data of the document.
     *
     * @param string $name The data name.
     *
     * @return mixed Some data.
     *
     * @throws \InvalidArgumentException If the data name does not exists.
     */
    public function offsetGet($name)
    {
        if (!isset(self::$dataCamelCaseMap[$name])) {
            throw new \InvalidArgumentException(sprintf('The data "%s" does not exists.', $name));
        }

        $method = 'get'.self::$dataCamelCaseMap[$name];

        return $this->$method();
    }

    /**
     * Throws a \LogicException because you cannot unset data in the document.
     *
     * @throws \LogicException
     */
    public function offsetUnset($name)
    {
        throw new \LogicException('You cannot unset data in the document.');
    }

    /**
     * Set data in the document.
     *
     * @param string $name  The data name.
     * @param mixed  $value The value.
     *
     * @return void
     *
     * @throws \InvalidArgumentException If the data name does not exists.
     */
    public function __set($name, $value)
    {
        if (!isset(self::$dataCamelCaseMap[$name])) {
            throw new \InvalidArgumentException(sprintf('The name "%s" does not exists.', $name));
        }

        $method = 'set'.self::$dataCamelCaseMap[$name];

        $this->$method($value);
    }

    /**
     * Returns data of the document.
     *
     * @param string $name The data name.
     *
     * @return mixed Some data.
     *
     * @throws \InvalidArgumentException If the data name does not exists.
     */
    public function __get($name)
    {
        if (!isset(self::$dataCamelCaseMap[$name])) {
            throw new \InvalidArgumentException(sprintf('The data "%s" does not exists.', $name));
        }

        $method = 'get'.self::$dataCamelCaseMap[$name];

        return $this->$method();
    }

    /**
     * Returns the data map.
     *
     * @return array The data map.
     */
    static public function getDataMap()
    {
        return array(
            'fields' => array(
                'usuarios_id' => array(
                    'type' => 'integer',
                    'required' => true,
                ),
                'nombre' => array(
                    'type' => 'string',
                    'required' => true,
                ),
                'descripcion' => array(
                    'type' => 'string',
                    'length' => 1024,
                    'required' => false,
                ),
                'activo' => array(
                    'type' => 'boolean',
                    'required' => false,
                ),
                'created_at' => array(
                    'type' => 'date',
                ),
                'updated_at' => array(
                    'type' => 'date',
                ),
                'identifier' => array(
                    'type' => 'integer',
                ),
            ),
            'references' => array(

            ),
            'embeddeds' => array(

            ),
            'relations' => array(

            ),
        );
    }
}
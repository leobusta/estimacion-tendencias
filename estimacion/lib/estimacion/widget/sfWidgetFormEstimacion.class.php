<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sfWidgetFormEstimacionclass
 *
 * @author pabloadi
 */
class sfWidgetFormEstimacion extends sfWidgetForm {
    //put your code here
    protected function configure(array $options, array $attributes) {
        $this->addOption('agent');
        parent::configure($options, $attributes);
    }

    public function render($name, $value = null, $attributes = array(), $errors = array()){

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url','Tag'));
        $agent=$this->getOption('agent');

        

        $boton="";
        
        if(preg_match('#^.*(Android|Mobile|Jasmine|Symbian|NetFront|BlackBerry|Opera Mini).*$#i',$agent)){
            $boton=$this->renderTag('input', array_merge(array('type' => "text", 'name' => $name, 'value' => $value), $attributes))."&nbsp;".sprintf("<a href='%s' data-rel='dialog' data-transition='pop'>Click para estimar</a>",url_for('historias/estimacion'));
        }
        else{
            $dialog=<<<EOF
<script type="text/javascript">
    $(function(){
        $('#ventana_estimacion').dialog({
            autoOpen: false,
            height: 350,
            width: 640,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    estimacion=$('input[name="esfuerzo"]').val();
                    if ( !isNaN(estimacion) && estimacion!="" ) {
                        $('#historias_tiempo_estimado').val(parseInt(estimacion));
                        $('#ventana_estimacion').dialog( "close" );
                    }},
                Cancel: function() {
                    $('#ventana_estimacion').dialog( 'close' );
                }},
        });
    });
</script>
EOF;

        $onclick=sprintf(<<<EOF
$.ajax({
    url: '%s',
    scripts: true,
    success: function(data) {
        $('#ventana_estimacion').html(data);
        if(jQuery.browser.mobile){
            //aqui va para iniciar la ventana en movil
            $.mobile.changePage('#ventana_estimacion', {transition: 'pop', role: 'dialog'});
            
            }else{
                //ventana para aplicacion web
                $('#ventana_estimacion').dialog('open');
            }
      }
});
EOF
                ,url_for('historias/estimacion'));

            $boton=$dialog.$this->renderTag('input', array_merge(array('type' => "text", 'name' => $name, 'value' => $value), $attributes))."&nbsp;"."<a href='#' onclick=\"$onclick\">Click para estimar</a>";
        }


        

        return $boton;
    }
}
?>

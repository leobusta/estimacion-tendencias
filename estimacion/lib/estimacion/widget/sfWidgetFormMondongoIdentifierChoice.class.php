<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sfWidgetFormMondongoIdentifierChoiceclass
 *
 * @author pabloadi
 */
class sfWidgetFormMondongoIdentifierChoice extends sfWidgetFormMondongoChoice {
    /**
   * Returns the choices associated to the model.
   *
   * @return array An array of choices
   */
  public function getChoices()
  {
    $choices = array();
    if (false !== $this->getOption('add_empty'))
    {
      $choices[''] = true === $this->getOption('add_empty') ? '' : $this->getOption('add_empty');
    }

    $method  = $this->getOption('method');
    foreach ((array) sfContext::getInstance()->get('mondongo')
      ->getRepository($this->getOption('model'))
      ->find($this->getOption('find_options'))
    as $document)
    {
      $choices[(string) $document->getIdentifier()] = $document->$method();
    }

    return $choices;
  }
}
?>

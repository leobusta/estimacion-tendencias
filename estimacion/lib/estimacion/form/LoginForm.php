<?php

/**
 * Login Form.
 */
class LoginForm extends BaseForm
{
    public function configure() {
        $this->setWidgets(array(
            'email'       => new sfWidgetFormInputText(),
            'password'    => new sfWidgetFormInputPassword(),
        ));

        $this->setValidators(array(
            'email'       => new sfValidatorEmail(array(), array(
                'required' => 'El email es requerido',
                'invalid' => 'Email es invalido'
            )),
            'password'    => new sfValidatorString(array('min_length' => 4), array(
                'min_length' => 'Escriba una contraseña mayor a 4 caracteres',
                'required' => 'La contraseña es requerida'
            ))
        ));

        $this->widgetSchema->setNameFormat('login[%s]');

        
    }

}

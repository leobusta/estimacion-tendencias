<?php

/**
 * Usuarios Form.
 */
class UsuariosForm extends BaseUsuariosForm
{
    public function configure() {

        $this->widgetSchema['password']=new sfWidgetFormInputPassword();
        $this->widgetSchema['password2']=new sfWidgetFormInputPassword();
        $this->widgetSchema['password2']->setLabel('De nuevo password');

        $this->validatorSchema['nombre']=new sfValidatorString(array('min_length' => 4), array(
            'required' => 'El nombre es requerido',
            'min_length' => 'Escriba un nombre mayor a 4 caracteres'
            ));
        $this->validatorSchema['email']=new sfValidatorEmail(array(), array(
            'required' => 'El email es requerido',
            'invalid' => 'Email es invalido'
            ));
        $this->validatorSchema['password']=new sfValidatorString(array('min_length' => 4), array(
            'min_length' => 'Escriba una contraseña mayor a 4 caracteres',
            'required' => 'La contraseña es requerida'
            ));

        $this->validatorSchema['password2']=new sfValidatorString(array('min_length' => 4), array(
            'min_length' => 'Escriba una contraseña mayor a 4 caracteres',
            'required' => 'La contraseña es requerida'
            ));

        $this->validatorSchema->setPostValidator(new sfValidatorSchemaCompare('password', '==', 'password2', array(), array(
            'invalid'   =>  'Las contraseñas no son iguales, intentelo de nuevo'
        )));

        $this->setOption('allow_extra_fields', true);

        unset($this['created_at'],$this['updated_at'],$this['identifier']);

    }
}
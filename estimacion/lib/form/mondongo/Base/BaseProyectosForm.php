<?php

/**
 * Proyectos Base Form.
 */
class BaseProyectosForm extends BaseFormMondongo
{

    /**
     * @see sfForm
     */
    public function setup()
    {
        $this->setWidgets(array(
            'usuarios_id' => new sfWidgetFormInputText(array(), array()),
            'nombre' => new sfWidgetFormInputText(array(), array()),
            'descripcion' => new sfWidgetFormInputText(array(), array()),
            'activo' => new sfWidgetFormInputCheckbox(array(), array()),
            'created_at' => new sfWidgetFormDateTime(array(), array()),
            'updated_at' => new sfWidgetFormDateTime(array(), array()),
            'identifier' => new sfWidgetFormInputText(array(), array()),

        ));

        $this->setValidators(array(
            'usuarios_id' => new sfValidatorInteger(array(), array()),
            'nombre' => new sfValidatorString(array(), array()),
            'descripcion' => new sfValidatorString(array(), array()),
            'activo' => new sfValidatorBoolean(array(), array()),
            'created_at' => new sfValidatorDateTime(array(), array()),
            'updated_at' => new sfValidatorDateTime(array(), array()),
            'identifier' => new sfValidatorInteger(array(), array()),

        ));

        $this->widgetSchema->setNameFormat('proyectos[%s]');
    }

    /**
     * @see sfMondongoForm
     */
    public function getModelName()
    {
        return 'Proyectos';
    }
}
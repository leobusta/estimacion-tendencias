<?php

/**
 * Historias Base Form.
 */
class BaseHistoriasForm extends BaseFormMondongo
{

    /**
     * @see sfForm
     */
    public function setup()
    {
        $this->setWidgets(array(
            'proyectos_id' => new sfWidgetFormInputText(array(), array()),
            'autor' => new sfWidgetFormInputText(array(), array()),
            'modulo' => new sfWidgetFormInputText(array(), array()),
            'nombre_hu' => new sfWidgetFormInputText(array(), array()),
            'identificador' => new sfWidgetFormInputText(array(), array()),
            'dependencia' => new sfWidgetFormInputText(array(), array()),
            'responsables' => new sfWidgetFormInputText(array(), array()),
            'actores' => new sfWidgetFormInputText(array(), array()),
            'iteracion' => new sfWidgetFormInputText(array(), array()),
            'tiempo_estimado' => new sfWidgetFormInputText(array(), array()),
            'tiempo_real' => new sfWidgetFormInputText(array(), array()),
            'descripcion' => new sfWidgetFormInputText(array(), array()),
            'observaciones' => new sfWidgetFormInputText(array(), array()),
            'created_at' => new sfWidgetFormDateTime(array(), array()),
            'updated_at' => new sfWidgetFormDateTime(array(), array()),
            'identifier' => new sfWidgetFormInputText(array(), array()),

        ));

        $this->setValidators(array(
            'proyectos_id' => new sfValidatorInteger(array(), array()),
            'autor' => new sfValidatorString(array(), array()),
            'modulo' => new sfValidatorString(array(), array()),
            'nombre_hu' => new sfValidatorString(array(), array()),
            'identificador' => new sfValidatorString(array(), array()),
            'dependencia' => new sfValidatorString(array(), array()),
            'responsables' => new sfValidatorString(array(), array()),
            'actores' => new sfValidatorString(array(), array()),
            'iteracion' => new sfValidatorInteger(array(), array()),
            'tiempo_estimado' => new sfValidatorInteger(array(), array()),
            'tiempo_real' => new sfValidatorInteger(array(), array()),
            'descripcion' => new sfValidatorString(array(), array()),
            'observaciones' => new sfValidatorString(array(), array()),
            'created_at' => new sfValidatorDateTime(array(), array()),
            'updated_at' => new sfValidatorDateTime(array(), array()),
            'identifier' => new sfValidatorInteger(array(), array()),

        ));

        $this->widgetSchema->setNameFormat('historias[%s]');
    }

    /**
     * @see sfMondongoForm
     */
    public function getModelName()
    {
        return 'Historias';
    }
}
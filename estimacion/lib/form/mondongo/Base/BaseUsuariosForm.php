<?php

/**
 * Usuarios Base Form.
 */
class BaseUsuariosForm extends BaseFormMondongo
{

    /**
     * @see sfForm
     */
    public function setup()
    {
        $this->setWidgets(array(
            'nombre' => new sfWidgetFormInputText(array(), array()),
            'email' => new sfWidgetFormInputText(array(), array()),
            'password' => new sfWidgetFormInputText(array(), array()),
            'created_at' => new sfWidgetFormDateTime(array(), array()),
            'updated_at' => new sfWidgetFormDateTime(array(), array()),
            'identifier' => new sfWidgetFormInputText(array(), array()),

        ));

        $this->setValidators(array(
            'nombre' => new sfValidatorString(array(), array()),
            'email' => new sfValidatorString(array(), array()),
            'password' => new sfValidatorString(array(), array()),
            'created_at' => new sfValidatorDateTime(array(), array()),
            'updated_at' => new sfValidatorDateTime(array(), array()),
            'identifier' => new sfValidatorInteger(array(), array()),

        ));

        $this->widgetSchema->setNameFormat('usuarios[%s]');
    }

    /**
     * @see sfMondongoForm
     */
    public function getModelName()
    {
        return 'Usuarios';
    }
}
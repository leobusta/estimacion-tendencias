<?php

/**
 * Historias Form.
 */
class HistoriasForm extends BaseHistoriasForm
{
    public function configure() {

         sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url','Tag'));


        $this->widgetSchema['descripcion']=new sfWidgetFormTextareaTinyMCE(array(
          'width'  => 550,
          'height' => 350,
          'config' => 'theme_advanced_disable: "anchor,image,cleanup,help"',
        ));

        $this->widgetSchema['observaciones']=new sfWidgetFormTextareaTinyMCE(array(
          'width'  => 550,
          'height' => 350,
          'config' => 'theme_advanced_disable: "anchor,image,cleanup,help"',
        ));

        $this->widgetSchema['identifier']=new sfWidgetFormInputHidden();
        $this->widgetSchema['tiempo_estimado']=new sfWidgetFormEstimacion(array('agent'=>$this->getOption('agent')));
        $this->widgetSchema['proyectos_id']=new sfWidgetFormInputHidden();

        $this->widgetSchema['dependencia']->setLabel('Dependencias');
        $this->widgetSchema['nombre_hu']->setLabel('Nombre historia');
        $this->widgetSchema['iteracion']->setLabel('Iteración');
        $this->widgetSchema['descripcion']->setLabel('Descripción');


        $this->setValidators(array(
            'proyectos_id' => new sfValidatorInteger(array('required'=>true)),
            'autor' => new sfValidatorString(array('required'=>true)),
            'modulo' => new sfValidatorString(array('required'=>true)),
            'nombre_hu' => new sfValidatorString(array('required'=>true)),
            'identificador' => new sfValidatorString(array('required'=>true)),
            'dependencia' => new sfValidatorString(array('required'=>false)),
            'responsables' => new sfValidatorString(array('required'=>false)),
            'actores' => new sfValidatorString(array('required'=>false)),
            'iteracion' => new sfValidatorInteger(array('required'=>false)),
            'tiempo_estimado' => new sfValidatorInteger(array('required'=>false)),
            'tiempo_real' => new sfValidatorInteger(array('required'=>false)),
            'descripcion' => new sfValidatorString(array('required'=>false)),
            'observaciones' => new sfValidatorString(array('required'=>false)),
            'created_at' => new sfValidatorDateTime(array('required'=>false)),
            'updated_at' => new sfValidatorDateTime(array('required'=>false)),
            'identifier' => new sfValidatorInteger(array('required'=>false)),

        ));

        unset($this['created_at'],$this['updated_at']);
        if($this->isNew())
                $this->setDefault('proyectos_id', $this->getOption('proyectos_id'));
                unset($this['identifier']);
    }
}

<?php

/**
 * Proyectos Form.
 */
class ProyectosForm extends BaseProyectosForm
{
    public function configure() {

        $this->widgetSchema['usuarios_id']=new sfWidgetFormInputHidden();
        $this->widgetSchema['identifier']=new sfWidgetFormInputHidden();
        $this->widgetSchema['descripcion']=new sfWidgetFormTextareaTinyMCE(array(
          'width'  => 550,
          'height' => 350,
          'config' => 'theme_advanced_disable: "anchor,image,cleanup,help"',
        ));
        
        $this->validatorSchema['nombre']=new sfValidatorString(array('min_length' => 4), array(
            'required' => 'El nombre es requerido',
            'min_length' => 'Escriba un nombre mayor a 4 caracteres'
            ));

        if($this->isNew()){
            $this->setDefault('usuarios_id', $this->getOption('usuarios_id'));
	    //$this->setDefault('identifier', rand(0,1000000));
            unset($this['identifier']);
	}

        unset($this['created_at'],$this['updated_at']);
    }
}

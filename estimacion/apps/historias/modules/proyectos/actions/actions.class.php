<?php

/**
 * proyectos actions.
 *
 * @package    estimacion
 * @subpackage proyectos
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class proyectosActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
      
      $this->proyectos=$this->getRepositorio()->find(array(
         'query' => array(
             'usuarios_id' => (int) $this->getUser()->getAttribute('usuario')->getIdentifier()
         )));
      
     
  }

  public function executeNew(sfWebRequest $request){
      $this->form=new ProyectosForm(null, array('usuarios_id'=> (int) $this->getUser()->getAttribute('usuario')->getIdentifier()));
      $this->proyecto=$this->form->getDocument();

  }

  public function executeCreate(sfWebRequest $request){
      
      $this->form=new ProyectosForm(null, array('usuarios_id'=> (int) $this->getUser()->getAttribute('usuario')->getIdentifier()));
      $this->proyecto=$this->form->getDocument();
      
      $this->processForm($request, $this->form);

      $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request){

      $this->proyecto=$this->getRepositorio()->findOne(array(
            'query' =>  array(
                'identifier' => (int) $request->getParameter('identifier')
            ),

      ));

      $this->forward404Unless($this->proyecto,'No existe el proyecto con el id '.$request->getParameter('identifier'));

      $this->form=new ProyectosForm($this->proyecto);

      if ($request->isMethod('post')){
          $this->processForm($request, $this->form);
      }


  }

  public function executeUpdate(sfWebRequest $request){

      $proyecto=$request->getParameter('proyectos');

      $this->proyecto=$this->getRepositorio()->findOne(array(
            'query' =>  array(
                'identifier' => (int) $proyecto['identifier']
            ),

      ));

      $this->forward404Unless($this->proyecto,'No existe el proyecto con el id '.$proyecto['identifier']);

      $this->form=new ProyectosForm($this->proyecto);
      $this->processForm($request, $this->form);
      
      $this->setTemplate('edit');
  }

  protected function processForm(sfWebRequest $request, sfForm $form){
      
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {
          $notice = $form->isNew() ? 'El elemento fue creado satisfactoriamente' : 'El elemento fue actualizado satisfactoriamente';

          try{
              $object = $form->save();
           } catch(Exception $e){
                $this->getUser()->setFlash('error', $e->getMessage());
                return sfView::SUCCESS;
          }
        
          $this->getUser()->setFlash('notice', $notice);

          $this->redirect("@proyectos_edit?identifier=".$object->getIdentifier());

        }
     else
        {
          $this->getUser()->setFlash('error', 'El elemento no ha sido guardado debido a errores', false);
        }
  }

  

  protected function getRepositorio(){
      $mondongo = $this->getMondongo();
      return $mondongo->getRepository('Proyectos');
  }

 
}

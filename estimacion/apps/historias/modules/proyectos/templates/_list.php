<div id="sf_admin_container">
  <div class="sf_admin_list">
  <table>
    <thead>
        <tr class="sf_admin_row">
          <!-- th id="sf_admin_list_batch_actions"><input type="checkbox" onclick="checkAll();" id="sf_admin_list_batch_checkbox"></th -->
          <th class="sf_admin_text">
	      Id  </th>
	<th class="sf_admin_text">
	      Nombre  </th>
	<th class="sf_admin_text">
	      Fecha de creaci&oacute;n  </th>
         <th id="sf_admin_list_th_actions">Acciones</th>
       </tr>
    </thead>
    <?php foreach($proyectos as $proyecto): ?>
    <tr >
        <td><?php echo $proyecto->getIdentifier() ?></td>
        <td><?php echo $proyecto->getNombre()?></td>
        <td><?php echo $proyecto->getCreatedAt()->format('d-m-Y') ?></td>
        <td><ul class="sf_admin_td_actions">
		<li class="sf_admin_action_edit"> <a data-role="button" href="<?php echo url_for("@proyectos_edit?identifier=".$proyecto->getIdentifier()) ?>">Editar</a></li>
	        <li class="sf_admin_action_show"><a data-role="button" href="<?php echo url_for("@historias?proyecto=".$proyecto->getIdentifier()) ?>">Historias</a></li>
	</ul>
     </td>
    </tr>
    <?php endforeach; ?>
</table>
</div>
</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
/* ]]> */
</script>

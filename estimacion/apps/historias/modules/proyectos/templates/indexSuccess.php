<?php use_helper('I18N', 'Date') ?>
<?php include_partial('proyectos/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Proyectos actuales', array(), 'messages') ?></h1>

  <?php include_partial('proyectos/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('proyectos/list_header') ?>
  </div>

  <div id="sf_admin_content">
     <?php include_partial('proyectos/list', array('proyectos'=>$proyectos)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('proyectos/list_actions') ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('proyectos/list_footer') ?>
  </div>
</div>

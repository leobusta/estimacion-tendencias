<?php use_helper('I18N', 'Date') ?>
<?php include_partial('proyectos/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Nuevo proyecto', array(), 'messages') ?></h1>

  <?php include_partial('proyectos/flashes') ?>

  <div id="sf_admin_header">
      <?php include_partial('proyectos/form_header', array('proyecto' => $proyecto, 'form' => $form)) ?>
  </div>

  <div id="sf_admin_content">
    <?php echo form_tag('proyectos/create') ?>
        <?php include_partial('proyectos/form', array('proyecto' => $proyecto, 'form' => $form)) ?>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('proyectos/form_footer', array('proyecto' => $proyecto, 'form' => $form)) ?>
  </div>
</div>

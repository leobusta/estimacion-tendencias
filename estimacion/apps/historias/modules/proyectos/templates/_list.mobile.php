<div id="sf_admin_container">
  <div class="sf_admin_list">
        <ul data-role="listview" data-split-icon="arrow-r" data-split-theme="d">
    <?php foreach($proyectos as $proyecto): ?>
        <li><a href="<?php echo url_for("@proyectos_edit?identifier=".$proyecto->getIdentifier()) ?>"><?php echo $proyecto->getNombre() ?></a> <a data-role="" href="<?php echo url_for("@historias?proyecto=".$proyecto->getIdentifier()) ?>">Historias</a></li>
    <?php endforeach; ?>
	</ul>
</table>
</div>
</div>

<?php

/**
 * historias actions.
 *
 * @package    estimacion
 * @subpackage historias
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class historiasActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
        $this->historias=$this->getRepositorio()->find(array(
         'query' => array(
             'proyectos_id' => (int) $request->getParameter('proyecto')
         )));

        $this->getUser()->setAttribute('proyecto', $this->getMondongo()->getRepository('Proyectos')->findOne(array('query' => array('identifier'=> (int) $request->getParameter('proyecto') ))));
  }

  public function executeNew(sfWebRequest $request){
      
      $this->form=new HistoriasForm(null, array('proyectos_id'=> (int) $this->getUser()->getAttribute('proyecto')->getIdentifier(), 'agent' =>$request->getHttpHeader('User-Agent')));
      $this->historia=$this->form->getDocument();

  }

  public function executeCreate(sfWebRequest $request){

      $this->form=new HistoriasForm(null, array('proyectos_id'=> (int) $this->getUser()->getAttribute('proyecto')->getIdentifier(), 'agent' =>$request->getHttpHeader('User-Agent')));
      $this->historia=$this->form->getDocument();

      $this->processForm($request, $this->form);

      $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request){

      $this->historia=$this->getRepositorio()->findOne(array(
            'query' =>  array(
                'identifier' => (int) $request->getParameter('identifier')
            ),

      ));

      $this->forward404Unless($this->historia,'No existe el historia con el id '.$request->getParameter('identifier'));

      $this->form=new HistoriasForm($this->historia, array('agent' =>$request->getHttpHeader('User-Agent')));

      if ($request->isMethod('post')){
          $this->processForm($request, $this->form);
      }


  }

  public function executeUpdate(sfWebRequest $request){

      $historia=$request->getParameter('historias');

      $this->historia=$this->getRepositorio()->findOne(array(
            'query' =>  array(
                'identifier' => (int) $historia['identifier']
            ),

      ));

      $this->forward404Unless($this->historia,'No existe el historia con el id '.$historia['identifier']);

      $this->form=new HistoriasForm($this->historia, array('agent' =>$request->getHttpHeader('User-Agent')));
      $this->processForm($request, $this->form);

      $this->setTemplate('edit');
  }

  public function executeSearch(sfWebRequest $request){
      $this->historias=$this->getRepositorio()->find(array(
         'query' => array(
             'nombre_hu' => (string) $request->getParameter('q')
         )));

      return $this->renderPartial('list', array('historias'=>$this->historias));
  }

  protected function processForm(sfWebRequest $request, sfForm $form){

      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {
          $notice = $form->isNew() ? 'El elemento fue creado satisfactoriamente' : 'El elemento fue actualizado satisfactoriamente';

          try{
              $object = $form->save();
           } catch(Exception $e){
                $this->getUser()->setFlash('error', $e->getMessage());
                return sfView::SUCCESS;
          }

          $this->getUser()->setFlash('notice', $notice);

          $this->redirect("@historias_edit?identifier=".$object->getIdentifier());

        }
     else
        {
          $this->getUser()->setFlash('error', 'El elemento no ha sido guardado debido a errores', false);
        }
  }

  public function executeEstimacion(){


      $this->a=sfConfig::get('app_historias_A');
      $this->b=sfConfig::get('app_historias_B');
      $this->peso_entrada_sistema=sfConfig::get('app_historias_entradasistema');
      $this->peso_salida_sistema=sfConfig::get('app_historias_salidasistema');
      $this->peso_entidades_relacional=sfConfig::get('app_historias_entidadesrelacional');
      $this->peso_interfaces_externos=sfConfig::get('app_historias_interfasesexternos');
      $this->peso_interfaces_protocolo=sfConfig::get('app_historias_interfasesprotocolo');

	//$this->setLayout("layoutmin");
  }


  protected function getRepositorio(){
      $mondongo = $this->getMondongo();
      return $mondongo->getRepository('Historias');
  }
}

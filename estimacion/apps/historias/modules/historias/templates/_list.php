<div id="sf_admin_container">
  <div class="sf_admin_list">
  <table>
    <thead>
        <tr class="sf_admin_row">
          <!-- th id="sf_admin_list_batch_actions"><input type="checkbox" onclick="checkAll();" id="sf_admin_list_batch_checkbox"></th -->
          <th class="sf_admin_text">
              Id  </th>
        <th class="sf_admin_text">
              Nombre  </th>
        <th class="sf_admin_text">
              Fecha de creaci&oacute;n  </th>
         <th id="sf_admin_list_th_actions">Acciones</th>
       </tr>
    </thead>

    <?php foreach($historias as $historia): ?>
    <tr>
        <td><?php echo $historia->getIdentifier() ?></td>
        <td><?php echo $historia->getNombreHu()?></td>
        <td><?php echo $historia->getCreatedAt()->format('d-m-Y') ?></td>
        <td><ul  class="sf_admin_td_actions">
	      <li class="sf_admin_action_edit">
 		<a data-role="button" href="<?php echo url_for("@historias_edit?identifier=".$historia->getIdentifier()) ?>">Editar</a>
	    </ul>
	</td>        
    </tr>
    <?php endforeach; ?>
</table>
</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
/* ]]> */
</script>

<div id="estima" data-role="dialog">
    <div data-role="header">
        <h1>Estimación Webmod++</h1>
        <style>

#estimacion input[type="text"] {
    width: 3em !important;
}
#estimacion table {
    color: black !important;
}

#estimacion table tr {
    text-align: left !important;
}
        </style>
        <script type="text/javascript">
            $(function(){
                inicializar();
            });
        </script>
    </div>
    <div id="estimacion" data-role="content">
        		<div id="interaccion_funcional" title="Interacción funcional y persistencia">
			<table border="0" cellpadding="1" cellspacing="1">
				<thead>
					<tr>
						<th scope="col" style="text-align: left;">
							M&eacute;trica</th>
						<th scope="col" style="text-align: left;">
							Prob.</th>
						<th scope="col" style="text-align: left;">
							Opti.</th>
						<th scope="col" style="text-align: left;">
							Pesim.</th>
						<th scope="col" style="text-align: left;">
							Cant.</th>
						<th scope="col" style="text-align: left;">
							Peso</th>
						<th scope="col" style="text-align: left;">
							Valor</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							Entradas <br/> al Sistema</td>
						<td>
                                                    <input name="entrada_sistema_prob" class="entrada_sistema_cantidad" size="5" type="text" /></td>
						<td>
							<input name="entrada_sistema_opti" class="entrada_sistema_cantidad" size="5" type="text" /></td>
						<td>
							<input name="entrada_sistema_pesi" class="entrada_sistema_cantidad" size="5" type="text" /></td>
						<td>
							<input name="entrada_sistema_cant" class="entrada_sistema_valor" disabled="disabled" size="5" type="text"  /></td>
						<td>
                                                    <input name="entrada_sistema_peso" class="entrada_sistema_valor" value="<?php echo $peso_entrada_sistema ?>" size="5" type="text" disabled="disabled" /></td>
						<td>
							<input name="entrada_sistema_valor" class="tifpsa" size="5" type="text" disabled="disabled" /></td>
					</tr>
					<tr>
						<td>
							Salidas del <br/> Sistema</td>
						<td>
							<input name="salida_sistema_prob" class="salida_sistema_cantidad" size="5" type="text" /></td>
						<td>
							<input name="salida_sistema_opti" class="salida_sistema_cantidad" size="5" type="text" /></td>
						<td>
							<input name="salida_sistema_pesi" class="salida_sistema_cantidad" size="5" type="text" /></td>
						<td>
							<input name="salida_sistema_cant" size="5" class="salida_sistema_valor" disabled="disabled" type="text" /></td>
						<td>
							<input name="salida_sistema_peso" class="salida_sistema_valor" value="<?php echo $peso_salida_sistema ?>" size="5" type="text" disabled="disabled" /></td>
						<td>
							<input name="salida_sistema_valor" class="tifpsa" size="5" type="text" disabled="disabled" /></td>
					</tr>
					<tr>
						<td>
							Entidades del <br/> Modelo Relacional</td>
						<td>
							<input name="entidades_relacional_prob" class="entidades_relacional_cantidad" size="5" type="text" /></td>
						<td>
							<input name="entidades_relacional_opti" class="entidades_relacional_cantidad" size="5" type="text" /></td>
						<td>
							<input name="entidades_relacional_pesi" class="entidades_relacional_cantidad" size="5" type="text" /></td>
						<td>
							<input name="entidades_relacional_cant" class="entidades_relacional_valor" size="5" type="text" disabled="disabled" /></td>
						<td>
							<input name="entidades_relacional_peso" class="entidades_relacional_valor" value="<?php echo $peso_entidades_relacional ?>" size="5" type="text" disabled="disabled" /></td>
						<td>
							<input name="entidades_relacional_valor" class="tifpsa" size="5" type="text" disabled="disabled" /></td>
					</tr>
					<tr>
						<td>
							Interfases Sistemas <br/> Externos API</td>
						<td>
							<input name="interfaces_externos_prob" class="interfaces_externos_cantidad" size="5" type="text" /></td>
						<td>
							<input name="interfaces_externos_opti" class="interfaces_externos_cantidad" size="5" type="text" /></td>
						<td>
							<input name="interfaces_externos_pesi" class="interfaces_externos_cantidad" size="5" type="text" /></td>
						<td>
							<input name="interfaces_externos_cant" class="interfaces_externos_valor" size="5" type="text" disabled="disabled" /></td>
						<td>
							<input name="interfaces_externos_peso" class="interfaces_externos_valor" value="<?php echo $peso_interfaces_externos ?>" size="5" type="text" disabled="disabled" /></td>
						<td>
							<input name="interfaces_externos_valor" class="tifpsa" size="5" type="text" disabled="disabled" /></td>
					</tr>
					<tr>
						<td>
							Interfases Sistemas <br/> Protocolo</td>
						<td>
							<input name="interfaces_protocolo_prob" class="interfaces_protocolo_cantidad" size="5" type="text" /></td>
						<td>
							<input name="interfaces_protocolo_opti" class="interfaces_protocolo_cantidad" size="5" type="text" /></td>
						<td>
							<input name="interfaces_protocolo_pesi" class="interfaces_protocolo_cantidad" size="5" type="text" /></td>
						<td>
							<input name="interfaces_protocolo_cant" class="interfaces_protocolo_valor" size="5" type="text" disabled="disabled" /></td>
						<td>
							<input name="interfaces_protocolo_peso" class="interfaces_protocolo_valor" value="<?php echo $peso_interfaces_protocolo ?>" size="5" type="text" disabled="disabled" /></td>
						<td>
							<input name="interfaces_protocolo_valor" size="5" class="tifpsa" type="text" disabled="disabled" /></td>
					</tr>
				</tbody>
			</table>
			<div id="logica_generador">
                            <p>
                            <table>
                                <tr>
                                    <td> Total Integraci&oacute;n Funcional <br/> - Persistencia Sin Ajustar:</td>
                                    <td> <input name="tifpsa" size="5" type="text" disabled="disabled" /> </td>
                                </tr>
                            </table>


                            </p>
				<table border="0" cellpadding="1" cellspacing="1" height="93">
					<tbody>
						<tr>
							<td>
								L&oacute;gica de <br/> negocio asociada</td>
							<td>
								<input name="logica_negocio" type="radio"  value="2" />Muy alta 2</td>
							<td>
								<input name="logica_negocio" type="radio" value="1" />Alta 1</td>
							<td>
								<input name="logica_negocio" type="radio" value="0.5" />Media 0.5</td>
							<td>
								<input name="logica_negocio" type="radio" value="0.1" />Baja 0.1</td>
						</tr>
						<tr>
							<td>
								Ayuda mediante <br/> generador c&oacute;digo</td>
							<td>
								<input name="generador_codigo" type="radio" value="0.5" />Alta 0.5</td>
							<td>
								<input name="generador_codigo" type="radio" value="0.75" />Media 0.75</td>
							<td colspan="2">
								<input name="generador_codigo" type="radio" value="0.95" />Baja 0.95</td>
						</tr>

						<tr>
							<td>
								Asesores </td>
							<td>
								<input name="asesores" type="radio" value="0.5" />Ninguno 0.5</td>
							<td>
								<input name="asesores" type="radio" value="1" />1 a 3 - 1</td>
							<td>
								<input name="asesores" type="radio" value="2" />3 a 6 - 2</td>
							<td colspan="">
								<input name="asesores" type="radio" value="3" />Más de 6 - 3</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<p>
            <table>
                <tr>
                    <td> Total integraci&oacute;n <br/> Funcional - Persistencia:</td>
                    <td><input name="tifp" size="5" type="text" class="pw" disabled="disabled" /> </td>
                </tr>
            </table>

                        </p>
		<div id="tipo_pantalla" title="Tipo Pantalla">
			<table border="0" cellpadding="1" cellspacing="1" style="width: 500px;">
				<thead>
					<tr>
						<th scope="col">
							<input name="no_aplica" type="checkbox" />
                                                        No Aplica (1)</th>
						<th scope="col">
							Baja</th>
						<th scope="col">
							Media</th>
						<th scope="col">
							Alta</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							Maestro</td>
						<td>
							<input name="pantalla_maestro" type="radio" value="1.08" />1.08</td>
						<td>
							<input name="pantalla_maestro" type="radio" value="1.2" />1.2</td>
						<td>
							<input name="pantalla_maestro" type="radio" value="1.3" />1.3</td>
					</tr>
					<tr>
						<td>
							Maestro-detalle</td>
						<td>
							<input name="pantalla_maestrodetalle" type="radio" value="1.1" />1.1</td>
						<td>
							<input name="pantalla_maestrodetalle" type="radio" value="1.3" />1.3</td>
						<td>
							<input name="pantalla_maestrodetalle" type="radio" value="1.5" />1.5</td>
					</tr>
					<tr>
						<td>
							Proceso</td>
						<td>
							<input name="pantalla_proceso" type="radio" value="1.08" />1.08</td>
						<td>
							<input name="pantalla_proceso" type="radio" value="1.2" />1.2</td>
						<td>
							<input name="pantalla_proceso" type="radio" value="1.5" />1.5</td>
					</tr>
					<tr>
						<td>
							Interactiva gr&aacute;fica</td>
						<td>
							<input name="pantalla_intergrafica" type="radio" value="1.3" />1.3</td>
						<td>
							<input name="pantalla_intergrafica" type="radio" value="1.5" />1.5</td>
						<td>
							<input name="pantalla_intergrafica" type="radio" value="1.8" />1.8</td>
					</tr>
					<tr>
						<td>
							Reporte b&aacute;sico</td>
						<td>
							<input name="pantalla_rebasico" type="radio" value="1.03" />1.03</td>
						<td>
							<input name="pantalla_rebasico" type="radio" value="1.1" />1.1</td>
						<td>
							<input name="pantalla_rebasico" type="radio" value="1.3" />1.3</td>
					</tr>
					<tr>
						<td>
							Reporte gr&aacute;fico</td>
						<td>
							<input name="pantalla_regrafico" type="radio" value="1.3" />1.3</td>
						<td>
							<input name="pantalla_regrafico" type="radio" value="1.5" />1.5</td>
						<td>
							<input name="pantalla_regrafico" type="radio" value="1.8" />1.8</td>
					</tr>
					<tr>
						<td>
							Reporte mixto</td>
						<td>
							<input name="pantalla_remixto" type="radio" value="1.4" />1.4</td>
						<td>
							<input name="pantalla_remixto" type="radio" value="1.6" />1.6</td>
						<td>
							<input name="pantalla_remixto" type="radio" value="2" />2</td>
					</tr>
				</tbody>
			</table>
		</div>

		<table border="0" cellpadding="1" cellspacing="1" style="width: 500px;">
			<tbody>
				<tr>
					<td>
						Factor Complejidad Pantalla:</td>
					<td>
						<input name="ftp" size="5" type="text" class="pw" disabled="disabled" /></td>
				</tr>
				<tr>
					<td>
						Porcentaje de reutilizaci&oacute;n (%):</td>
					<td>
						<input name="porcentaje_reutilizacion" size="5" type="text" class="pw" /></td>
				</tr>
                                <tr>
					<td>
						Puntos web:</td>
					<td>
						<input name="puntos_web" size="5" type="text" class="pw" disabled="disabled" /></td>
				</tr>
				<tr>
					<td>
						Esfuerzo:</td>
					<td>
						<input name="esfuerzo" size="5" type="text" disabled="disabled" /></td>
				</tr>
			</tbody>
		</table>
        <div>
        <a href="#" data-role="button" data-rel="back" data-direction="reverse" data-theme="c">Cancel</a>
        <a id="estimacion_aceptar" href="#" data-role="button" data-rel="back" data-direction="reverse" data-theme="c">Aceptar</a>
        </div>
    </div>
</div>

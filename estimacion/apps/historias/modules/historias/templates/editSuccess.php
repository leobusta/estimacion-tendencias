<?php use_helper('I18N', 'Date') ?>
<?php include_partial('historias/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Editar historia', array(), 'messages') ?></h1>

  <?php include_partial('historias/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('historias/form_header', array('historia' => $historia, 'form' => $form )) ?>
  </div>

  <div id="sf_admin_content">
      <?php echo form_tag('historias/update') ?>
    <?php include_partial('historias/form', array('historia' => $historia, 'form' => $form)) ?>
        </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('historias/form_footer', array('historia' => $historia, 'form' => $form )) ?>
  </div>
</div>

<div id="ventana_estimacion" data-role="page" ></div>


<?php use_helper('I18N', 'Date') ?>
<?php include_partial('historias/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Historias del proyecto', array(), 'messages') ?></h1>

  <?php include_partial('historias/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('historias/list_header') ?>
  </div>

  <div id="sf_admin_content">
     <?php include_partial('historias/list', array('historias'=>$historias)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('historias/list_actions') ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('historias/list_footer') ?>
  </div>
</div>

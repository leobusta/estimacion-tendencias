<?php

/**
 * usuarios actions.
 *
 * @package    estimacion
 * @subpackage usuarios
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class usuariosActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }
  
  public function executeTest(sfWebRequest $request)
  {
    $usuario=new Usuarios();
    $usuario->nombre="Pablo Diaz";
    $usuario->login="pabloadi";
    $usuario->password="pabloadi";
    $usuario->email="pabloadi@gmail.com";
    $usuario->save();
    
    return $this->renderText("Done");
  }
}

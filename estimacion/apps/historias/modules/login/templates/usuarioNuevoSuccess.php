<div id="form-nuevousuario">
        <h1>Crear usuario</h1><br>
        <div>
            <form action="<?php echo url_for('login/usuarioNuevo') ?>" method="POST">
                <table width="100%">
                  <tbody>
                <?php echo $form ?>
                  </tbody>
                  <tfoot>
                    <tr>
                        <td colspan="2">
                        <a  data-inline="true" data-icon="arrow-l" data-role="button" href="<?php echo url_for('login/index') ?>" class="button medium blue">Regresar</a>
                        <input data-theme="b" type="submit" data-inline="true" value="Enviar" class="button medium blue">
                        </td>
                    </tr>
                </tfoot>
                </table>
            </form>
        </div>
</div>

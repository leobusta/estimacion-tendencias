<div id="form-login">
    
        <h1>Identificaci&oacute;n del usuario</h1><br />
        <div>
            <form action="<?php echo url_for('login/index') ?>" method="POST">
		<table width="100%">
		  <tbody>
                <?php echo $form ?>
		  </tbody>
		  <tfoot>
      		    <tr>
		        <td colspan="2">
          		<input  data-theme="b" data-inline="true" type="submit" value="Ingresar" class="button medium blue">
                        <a  data-inline="true" href="<?php echo url_for('login/usuarioNuevo') ?>" data-role="button" class="button medium blue" >Registrarse</a>
                  	</td>
      		    </tr>
    		</tfoot>
		</table>
            </form>
        </div>
</div>

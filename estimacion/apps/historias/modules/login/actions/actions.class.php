<?php

/**
 * login actions.
 *
 * @package    estimacion
 * @subpackage login
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class loginActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
//      if(!$this->getUser()->isAuthenticated()){
//        $this->redirect('proyectos/index');
//      }

      $this->form=new LoginForm();

      if ($request->isMethod('post'))
      {
          $this->form->bind($request->getParameter('login'), $request->getFiles('login'));
          if ($this->form->isValid())
          {
            if ($this->consultarUsuario($request))
            {
              
              $this->getUser()->setAuthenticated(true);

              $this->redirect('proyectos/index');
            }
            else{
                $this->getUser()->setFlash('error', 'El usuario o la clave no coinciden');
            }

          }

      }

  }

  /**
   * Función privada para hacer la conexión con Mongodb y consultar el usuario
   * @param sfWebRequest $request
   */
  private function consultarUsuario(sfWebRequest $request){
      $mondongo = $this->getMondongo();
      $usuarios_rep = $mondongo->getRepository('Usuarios');

      $this->form=new LoginForm();
      $this->form->bind($request->getParameter('login'), $request->getFiles('login'));
      $login=$this->form->getValues();

      MongoCursor::$timeout = -1;

      $usuario = $usuarios_rep->findOne(array(
        'query' => array(
            'email' => $login['email'],
            'password' => $login['password']
            )
      ));

      if($usuario){
          $this->getUser()->setAttribute('usuario', $usuario);
          return true;
      }
      else
          return false;
  }

  /**
   * Quita la autenticación del usuario y borra todos sus datos de sesión
   */
  public function executeLogout(){
      $this->getUser()->setAuthenticated(false);
      $this->getUser()->getAttributeHolder()->clear();
      $this->getUser()->setFlash('notice', 'Ha salido de la sesion');

      $this->redirect('login/index');

  }

  /**
   * Formulario para crear un usuario nuevo
   * @param sfWebRequest $request
   */
  public function executeUsuarioNuevo(sfWebRequest $request){
      $this->form=new UsuariosForm();
      if ($request->isMethod('post'))
        {
          $this->form->bind($request->getParameter('usuarios'), $request->getFiles('usuarios'));
          if ($this->form->isValid())
          {
            $usuario=$this->form->getValues();

            if(!$this->verificarCorreo($usuario['email'])){
                $this->form->save();
                $this->getUser()->setFlash('notice', 'Usuario creado satisfactoriamente');
                $this->redirect('login/index');
            }
            else{
                $this->getUser()->setFlash('error', 'Ya existe un usuario con ese email');
            }
          }
        }
  }

  /**
   * Función privada para hacer la conexión con Mongodb y consultar el correo
   * @param string email
   */
  private function verificarCorreo($email){
      $mondongo = $this->getMondongo();
      $usuarios_rep = $mondongo->getRepository('Usuarios');

      $usuario = $usuarios_rep->find(array(
        'query' => array(
            'email' => $email,
         ),
        'limit'  => 1
      ));

      if($usuario)
          return true;
      else
          return false;
  }

}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  <head>



    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <?php include_http_metas() ?>

    <?php include_metas() ?>

    <?php include_title() ?>

    <link rel="shortcut icon" href="/favicon.ico" />

    <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>

    <?php include_stylesheets() ?>

    <?php include_javascripts() ?>

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />


    <script src="/js/tiny_mce/tiny_mce.js"></script>

    <script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.js"></script>

    <link rel="stylesheet" href="/css/mobile.css" />
    <script src="/js/mobile.js"></script>
    

  </head>

    <body>

        <div id="<?php echo $_SERVER['REQUEST_URI']?>" data-role="page">

            <div data-theme="a" data-role="header">

                <h2>

                    <?php include_title() ?>

                </h2>

        <?php if ($sf_user->hasFlash('notice')): ?>

          <div class="flash_notice">

            <?php echo $sf_user->getFlash('notice') ?>

          </div>

        <?php endif; ?>

 

        <?php if ($sf_user->hasFlash('error')): ?>

          <div class="flash_error">

            <?php echo $sf_user->getFlash('error') ?>

          </div>

        <?php endif; ?>

                <div data-role="navbar" data-iconpos="left">

                    <ul>

                        <li>

                            <a href="<?php echo url_for('proyectos/index')  ?>" data-theme="a" data-icon="home" class="ui-btn-active ui-state-persist">

                                Proyectos

                            </a>

                        </li>

                    </ul>

                </div>

            </div>

            <div data-role="content">

                <?php echo $sf_content ?>

            </div>

	    <div data-role="footer"> 

		

	    </div> 

        </div>

        <script>

            //App custom javascript

        </script>

    </body>

</html>

	

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  <head>

    <?php include_http_metas() ?>

    <?php include_metas() ?>

    <?php include_title() ?>

    <link rel="shortcut icon" href="/favicon.ico" />

    <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

    <script src="/js/tiny_mce/tiny_mce.js"></script>

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/ui.all.css" />
    <link rel="stylesheet" href="/css/sf_admin.css" />
    <link rel="stylesheet" href="/css/boton.css" />
    <link rel="stylesheet" href="/css/sfadmin/default.css" />
    <link rel="stylesheet" href="/css/sfadmin/global.css" />
    <link rel="stylesheet" href="/css/tabs.css" />
    <link rel="stylesheet" href="/css/main.css" />

    <?php include_stylesheets() ?>

    <?php include_javascripts() ?>

  </head>

  <body>

    <div id="container" class="container_16">

      <div id="header" class="container_16">

        <div id="user_menu" class="grid_3">

          <?php echo link_to('Inicio', 'homepage') ?>

	        <?php if($sf_user->isAuthenticated()): ?>

	        | <?php echo $sf_user->getAttribute('usuario') ?> (<?php echo link_to('Salir','login/logout') ?>)

        	<?php endif ?>

        </div><!-- end user_menu -->

        <div class="content">

	<div id="logos">  

          <div id="logo"> 

                    <a href="/"><img src="/images/logo.png" align="left" height="100px" width="100px"></a>

          </div><h1></h1>

        </div> 

	</div>

      </div>



<div id="mainMenu">

  <ul>

    <li <?php echo (strpos($_SERVER['REQUEST_URI'],'home')!==FALSE)? 'id="current"' : '' ?>> <?php echo link_to('Inicio', 'homepage')?> </li>

    <li <?php echo (strpos($_SERVER['REQUEST_URI'],'proyectos')!==FALSE)? 'id="current"' : '' ?>><?php echo link_to('Proyectos','proyectos/index' )?>  </li>

    <li <?php echo (strpos($_SERVER['REQUEST_URI'],'historias')!==FALSE)? 'id="current"' : '' ?>><?php echo link_to('Historias de Usuario','historias/index' )?>  </li>

    <li <?php echo (strpos($_SERVER['REQUEST_URI'],'manual')!==FALSE)? 'id="current"' : '' ?>><?php echo link_to('Manual','manual_admin/index' )?>  </li>



  </ul>

</div><!-- end mainMenu -->



      <div id="content">

        <?php if ($sf_user->hasFlash('notice')): ?>

          <div class="flash_notice">

            <?php echo $sf_user->getFlash('notice') ?>

          </div>

        <?php endif; ?>

 

        <?php if ($sf_user->hasFlash('error')): ?>

          <div class="flash_error">

            <?php echo $sf_user->getFlash('error') ?>

          </div>

        <?php endif; ?>

 

        <div class="content">

          <?php echo $sf_content ?>

        </div>

      </div>

      <div id="footer">

        <div class="content">

          <span class="symfony">

	    Proyecto Tendencias

            powered by <a href="/">

            <img src="/images/symfony.gif" alt="symfony framework" />

            </a>

          </span>

          <ul>

            <li><a href="">Acerca de Nosotros</a></li>

            <li class="last"><a href=""></a></li>

          </ul>

        </div>

      </div>

  </body>

</html>



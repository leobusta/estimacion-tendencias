<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MANUAL  DE USUARIO DEL SITIO WEB ESTIMACION</title>
<link href="/favicon.ico" rel="shortcut icon">
<link href="/css/sf_admin.css" rel="stylesheet">
<link href="/css/boton.css" rel="stylesheet">
<link href="/css/sfadmin/default.css" rel="stylesheet">
<link href="/css/sfadmin/global.css" rel="stylesheet">
<link href="/css/tabs.css" rel="stylesheet">
<link href="/css/main.css" rel="stylesheet">
</head>

<body>
<div id="container" class="container_16" bgcolor="#FFF">

<div id="header" class="container_16" >
        <div id="user_menu" class="grid_3">

          <a href="/">Inicio</a>
	        
        </div><!-- end user_menu -->

        <div class="content">

	<div id="logos">  

          <div id="logo"> 

                    <a href="/"><img src="/images/logo.png" align="left" height="100px" width="100px"></a>

          </div><h1><p align="center"><strong>MANUAL  DE USUARIO DEL SITIO WEB ESTIMACION</strong><br />
  <a href="http://tendencias.calipso.com.co/">http://tendencias.calipso.com.co/</a></p></h1>

        </div> 

	</div>

   </div>  
<div id="mainMenu">
  <ul>

    <li <?php echo ($page=="ingreso")?"id='current'":"" ?> ><a href="/manual/index.php?page=ingreso">Inicio</a> </li>

    <li <?php echo ($page=="proyectos")?"id='current'":""?> ><a href="/manual/index.php?page=proyectos">Proyectos</a>  </li>

    <li <?php echo ($page=="historias")?"id='current'":""?> ><a href="/manual/index.php?page=historias">Historias de Usuario</a>  </li>
  </ul>
</div>

<div id="content">
<div class="content">
     <table border="0" cellpadding="20" cellspacing="20"  bgcolor="#FFF">
	<tr><td style="padding:10px">
	<?php  include("/home/sfprojects/tendencias/estimacion/web/manual/$page.html");?>
	</td></tr>
</div>
</div>
</div>
</body>
</html>
